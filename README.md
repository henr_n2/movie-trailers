# Movie Trailer

The application test for Occident made by Henrik Nilsson

### Installing

A step by step series of examples that tell you how to get a development env running

To start a local environment of the application first run the command

```
npm install
```

To get the node packages installed and then after run 

```
npm run start
```

With the node environment running you can access the application in your browser with `http://localhost:3000/`

## Built With

* [Create React App](https://github.com/facebook/create-react-app)
* [express](https://expressjs.com/) - Used to run the node environment
* [Material UI](https://material-ui.com/) - Components used in the react code
* [React Autosuggest](https://react-autosuggest.js.org/) - Used by the Searbar component
* [react-youtube](https://www.npmjs.com/package/react-youtube) - Used to play the youtube videos as a react component



## Authors

* **Henrik Nilsson** - [henr_n2](https://bitbucket.org/henr_n2/)

