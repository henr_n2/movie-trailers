import React, { Component } from 'react'
import './App.scss'
import { connect } from "react-redux"
import { fetchMovies } from "./actions/movieActions"
import Header from './components/header/Header'
import Sidebar from './components/sidebar/Sidebar'
import MovieListContainer from './containers/MovieListContainer';

class App extends Component {
   componentWillMount() {
      this.props.dispatch(fetchMovies());
   }
   render() {
      return (
         <div className="App">
            {/* Header ynly for the mobile profile view */}
            <Header genres={this.props.genres} types={this.props.types} />

            <main className="main-page">
               {/* Desktop sidebar menu */}
               <Sidebar genres={this.props.genres} types={this.props.types}/>
               <MovieListContainer />
            </main>
         </div>
      );
   }
}
const mapStateToProps = state => ({
   movies: state.movies.movies,
   genres: state.movies.genres ? state.movies.genres : [],
   types: state.movies.types ? state.movies.types : [],
   loading: state.movies.loading,
   error: state.movies.error
});

export default connect(mapStateToProps)(App);
