import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Menu from '../../containers/MenuContainer'

const styles = {
   list: {
     width: 250,
     padding: '20%',
   },
 };

class Header extends Component {
   state = {
      drawer: false,
   };

   /**
    * Toggles the drawer in mobile open or closed by setting the state value drawer.
    * 
    * @param {bool} open boolean, true or false, that is to be set as the drawer value in the state.
    * 
    * @return {function} A functions that in turn toggles the open state.
    */
   toggleDrawer = open => {
      return () => {
         this.setState({
            drawer: open,
         });
      }
   };

   render() {
      const { classes } = this.props;

      return (
         <header>
            <div className="mobile-header">
               <IconButton 
                  className={"menu-button"} 
                  aria-label="Menu" 
                  onClick={this.toggleDrawer(true)}
               >
                  <MenuIcon />
               </IconButton>
            </div>
            
            <SwipeableDrawer
               open={this.state.drawer}
               onClose={this.toggleDrawer(false)}
               onOpen={this.toggleDrawer(true)}  
            >
               <div
                  tabIndex={0}
                  role="button"
                  onClick={this.toggleDrawer(false)}
                  onKeyDown={this.toggleDrawer(false)}
                  className={`${classes.list} drawer-background`}
               >
                  <Menu />
               </div>
            </SwipeableDrawer>
         </header>
      )
   }
}
Header.propTypes = {
   classes: PropTypes.object.isRequired
};
 
export default withStyles(styles)(Header);