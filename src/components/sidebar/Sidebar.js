import React from 'react';
import Menu from '../../containers/MenuContainer'
import Logo from '../../styles/logo.png';

const Sidebar = () => {
   return (
      <div className="desktop-sidemenu">
         <img className="logo" src={Logo} alt={'movie trailers logo'}/>
         <Menu />
      </div>
   )
}
export default Sidebar;