import React, { Component } from 'react';
import PropTypes from 'prop-types';
import deburr from 'lodash/deburr';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';

import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';

function renderInputComponent(inputProps) {
   const { classes, inputRef = () => { }, ref, ...other } = inputProps;

   return (
      <TextField
         className={classes.inputField}
         fullWidth
         InputProps={{
            inputRef: node => {
               ref(node);
               inputRef(node);
            },
            classes: {
               input: classes.input,
            },
            startAdornment: (
               <InputAdornment className={classes.inputAdornment} position="start">
                  <SearchIcon />
               </InputAdornment>
            ),
         }}
         {...other}
      />
   );
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
   const matches = match(suggestion.label, query);
   const parts = parse(suggestion.label, matches);

   return (
      <MenuItem selected={isHighlighted} component="div">
         <div>
            {parts.map((part, index) => {
               return part.highlight ? (
                  <span key={String(index)} style={{ fontWeight: 500 }}>
                     {part.text}
                  </span>
               ) : (
                     <strong key={String(index)} style={{ fontWeight: 300 }}>
                        {part.text}
                     </strong>
                  );
            })}
         </div>
      </MenuItem>
   );
}
function getSuggestions(value, movieList) {
   const inputValue = deburr(value.trim()).toLowerCase();
   const inputLength = inputValue.length;
   let count = 0;

   return inputLength === 0
      ? []
      : movieList.filter(suggestion => {
         const keep =
            count < 5 && suggestion.label.slice(0, inputLength).toLowerCase() === inputValue;

         if (keep) {
            count += 1;
         }

         return keep;
      });
}

function getSuggestionValue(suggestion) {
   return suggestion.label;
}

const styles = theme => ({
   container: {
      position: 'relative',
   },
   inputField: {
      backgroundColor: '#FFF',
      fontSize: '12px'
   },
   input: {
      padding: '10px',
   },
   inputAdornment: {
      padding: '0 0 0 10px',
      color: '#0b171e',
   },
   suggestionsContainerOpen: {
      position: 'absolute',
      zIndex: 1,
      marginTop: theme.spacing.unit,
      left: 0,
      right: 0,
   },
   suggestion: {
      display: 'block',
   },
   suggestionsList: {
      margin: 0,
      padding: 0,
      listStyleType: 'none',
   },
   divider: {
      height: theme.spacing.unit * 2,
   },
});

class Searchbar extends Component {
   constructor(props) {
      super(props);
      this.state = {
         single: '',
         suggestions: [],
      };
   }
   handleSuggestionsFetchRequested = ({ value }) => {
      const movieList = this.props.movies.map(movie => {
         return { id: movie.id, label: movie.title }
      })
      this.setState({
         suggestions: getSuggestions(value, movieList),
      });
   };

   handleSuggestionsClearRequested = () => {
      this.setState({
         suggestions: [],
      });
   };

   handleChange = name => (event, { newValue }) => {
      this.setState({
         [name]: newValue,
      });
      this.props.setSearch(newValue);
   };

   render() {
      const { classes } = this.props;

      const autosuggestProps = {
         renderInputComponent,
         suggestions: this.state.suggestions,
         onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
         onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
         getSuggestionValue,
         renderSuggestion,
      };
      return (
         <div>
            <Autosuggest
               {...autosuggestProps}
               inputProps={{
                  classes,
                  placeholder: 'Search for your favorite movie',
                  value: this.state.single,
                  onChange: this.handleChange('single'),
               }}
               theme={{
                  container: classes.container,
                  suggestionsContainerOpen: classes.suggestionsContainerOpen,
                  suggestionsList: classes.suggestionsList,
                  suggestion: classes.suggestion,
               }}
               renderSuggestionsContainer={options => (
                  <Paper {...options.containerProps} square>
                     {options.children}
                  </Paper>
               )}
            />
         </div>
      )
   }
}

Searchbar.propTypes = {
   classes: PropTypes.object.isRequired,
   movies: PropTypes.array,
   setSearch: PropTypes.func,
};

export default withStyles(styles)(Searchbar);