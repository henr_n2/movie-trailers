import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Menu extends Component {
   /**
    * High-order function to run the redux action setGenre.
    * 
    * @param {int} id the id of the genre to be set as the filter.genre in the store.
    * 
    * @return {function} A functions that in turn runs the setGenre function.
    */
   setGenreFilter = id => {
      return () => {
         this.props.setPage(1, 12);
         this.props.setGenre(id);
      }
      
   }

   /**
    * High-order function to run the redux action setType.
    * 
    * @param {int} id the id of the genre to be set as the filter.type in the store.
    * 
    * @return {function} A functions that in turn runs the setType function.
    */
   setTypeFilter = id => {
      return () => {
         this.props.setPage(1, 12);
         this.props.setType(id);
      }
   }
   
   render() {
      const {genres, types} = this.props;
      return (
         <div className="menu">
            <div className="menu--categories">
               <h2 className="menu--title">Categories</h2>
               <nav className="menu--navigation">
                  {genres.map(genre => {
                     return (
                        <a 
                           key={genre.id}
                           className="menu--anchor"
                           onClick={this.setGenreFilter(genre.id)}
                        >{genre.title}</a>
                     )
                  })}
               </nav>
            </div>
            <div className="menu--types">
               <h2 className="menu--title">Type</h2>
               <nav className="menu--navigation">
                  {types.map(type => {
                        return (
                           <a 
                              key={type.id}
                              className="menu--anchor"
                              onClick={this.setTypeFilter(type.id)}
                           >{type.title}</a>
                        )
                     })}
               </nav>
            </div>
         </div>
      )
   }
}
Menu.propTypes = {
   genres: PropTypes.array,
   types: PropTypes.array,
};

export default Menu;