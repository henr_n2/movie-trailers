import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import YouTube from 'react-youtube';

function getModalStyle() {
   const top = 50;
   const left = 50;

   return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
   };
}

class TrailerModal extends Component {
   constructor(props) {
      super(props);

      this.state = {
         pagination: 1,
         currentMovies: 12,
         modalState: false,
      }
   }
   render() {
      const { classes, trailer, modalState, close } = this.props;
      const opts = {
         height: '100%',
         width: '100%',
         playerVars: { // https://developers.google.com/youtube/player_parameters
           autoplay: 1
         }
       };
       let id;
       if(modalState) {
         id = trailer.trailer.replace('https://www.youtube.com/watch?v=','');
       }
      return (
         <div className="trailer-modal">
            <Modal
               aria-labelledby="simple-modal-title"
               aria-describedby="simple-modal-description"
               open={modalState}
               onClose={close()}
            >
               <div style={getModalStyle()} className="trailer-modal--paper">
                  <div className="trailer-header">
                     <h2>{trailer && trailer.title}</h2>
                     <IconButton
                        onClick={close()}
                        className={classes.close}
                        aria-label="Delete"
                     >
                        <CloseIcon />
                     </IconButton>
                  </div>
                  <div className="trailer-video">
                     {id && (
                        <YouTube
                           videoId={id}
                           opts={opts}
                           onReady={this._onReady}
                        />
                     )}
                     
                  </div>
               </div>
            </Modal>
         </div>
      );
   }
}

TrailerModal.propTypes = {
   classes: PropTypes.object.isRequired,
   modalState: PropTypes.bool,
   trailer: PropTypes.object,
   close: PropTypes.func,
};

export default withStyles({})(TrailerModal);