import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import MovieListItem from './MovieListItem'
import SearchbarContainer from '../../containers/SearchbarContainer'
import TrailerModal from './TrailerModal'

class MovieList extends Component {
   constructor(props) {
      super(props);
      
      this.state = { 
         modalState: false,
         trailer: null
      }
   }
   /**
    * Sets the pagination forward one step.
    * 
    * @return {function} A functions that runs the setPage function.
    */
   paginationForward = () => {
      return () => {
         const page = this.props.pagination + 1;
         const rangeMax = this.props.rangeMax + 12;
         this.props.setPage(page,rangeMax);
      }
   }

   /**
    * Sets the pagination back one step.
    * 
    * @return {function} A functions that runs the setPage function.
    */
   paginationBack = () => {
      if(this.props.pagination > 1) {
         return () => {
            const page = this.props.pagination - 1;
            const rangeMax = this.props.rangeMax - 12;
            this.props.setPage(page,rangeMax);
         }
      }
   }

   /**
    * Sets the pagination by a value.
    * 
    * @param {int} value the page value to set by.
    * 
    * @return {function} A functions that runs the setPage function.
    */
   paginationSelect = value => {
      return () => {
         const rangeMax = 0 + (value * 12);
         this.props.setPage(value, rangeMax);
      }
   }

   /**
    * Sets the modal view to true and sets the trailer object to show
    * 
    * @param {object} movie the object of the trailer to set the modal view to.
    * 
    * @return {function} A functions that set the state to open the modal and set it to the selected object.
    */
   openTrailerModal = (movie) => {
      return () => {
         this.setState({
            ...this.state,
            modalState: true,
            trailer: movie
         })
      }
   }

   /**
    * Sets the trailer modal as false
    * 
    * @return {function} A functions that closes the modal view.
    */
   closeTrailerModal = () => {
      return () => {
         this.setState({
            ...this.state,
            modalState: false,
         })
      }
   }

   /**
    * Removes the genre filter.
    * 
    * @return {function} A functions that runs the removeGenreFilter action.
    */
   removeGenreFilter = () => {
      return () => {
         this.props.setPage(1, 12);
         this.props.removeGenreFilter();
      }
   }

   /**
    * Removes the type filter.
    * 
    * @return {function} A functions that runs the removeTypeFilter action.
    */
   removeTypeFilter = () => {
      return () => {
         this.props.setPage(1, 12);
         this.props.removeTypeFilter();
      }
   }

   render() {
      const {movies, types, genres, genreFilter, typeFilter, pagination} = this.props;

      // The amount of pages in the paginations
      const pageNumbers = [];
      for (let i = 1; i <= Math.ceil(movies.length / 12); i++) {
         pageNumbers.push(i);
      }
      // Index to make sure that the whole list is set with 12 list items
      let set = 0;
      // Arrays to see if any genres or types is set in the filter
      const selectedGenreFilter = genres.filter(genre => {
         return genre.id === genreFilter
      })
      const selectedTypeFilter = types.filter(type => {
         return type.id === typeFilter
      })
      return (
         <div className="movie-list">
            <SearchbarContainer />
            {/* Selected filters with delete filter buttons */}
            <div className="filtered">
               {genreFilter && (
                  <div className="filter">
                     <span className="filter--title">{selectedGenreFilter[0].title}</span>
                     <IconButton className="filter--close" onClick={this.removeGenreFilter()} >
                        <CloseIcon />
                     </IconButton>
                     
                  </div>
               )}
               {typeFilter && (
                  <div className="filter">
                     <span className="filter--title">{selectedTypeFilter[0].title}</span>
                     <IconButton className="filter--close" onClick={this.removeTypeFilter()} >
                        <CloseIcon/>
                     </IconButton>
                     
                  </div>
               )}
            </div>
            {/* Movie list grid */}
            <div className="movie-grid">
               {movies.map((movie,index) => {
                  if(index > (this.props.rangeMax - 13) && index < this.props.rangeMax) {
                     const movieGenres = genres.filter(genre => {
                           return movie.genre.includes(genre.id);
                     })
                     const movieTypes = types.filter(type => {
                        if (type.title !== 'All') {
                           return movie.type.includes(type.id);
                        }
                        
                     })
                     return (
                        <MovieListItem 
                           movie={movie} 
                           key={index} 
                           types={movieTypes} 
                           genres={movieGenres} 
                           selectTrailer={this.openTrailerModal}
                        />
                     )
                  } else if( index === movies.length && set < 13) {
                     return (
                        <MovieListItem/>
                     )
                  }
                  set++;
                  
               })}
            </div>
            {/* Trailer modal */}
            <TrailerModal 
               modalState={this.state.modalState}
               close={this.closeTrailerModal}
               trailer={this.state.trailer}
            />
            <div className="pagination">
               <button 
                  disabled={pagination === 1 ? true : false} 
                  className="pagination--button" 
                  onClick={this.paginationBack()}
               >
                  <ChevronLeft/>
               </button>
               <div className="pagination--numbers">
                  {pageNumbers.map(number => {
                     const selected = pagination === number ? true : false;
                     return (
                        <button 
                           key={number}
                           onClick={this.paginationSelect(number)} 
                           className={`${selected ? 'selected' : ''} pagination--button pagination--number`}
                        >
                           {number}
                        </button>
                     )
                  })}
               </div>
               <button 
                  disabled={pagination === pageNumbers.length ? true : false} 
                  className="pagination--button" 
                  onClick={this.paginationForward()}
               >
                  <ChevronRight/>
               </button>
            </div>
         </div>
      )
   }
}

MovieList.propTypes = {
   movies: PropTypes.array,
   types: PropTypes.array,
   genres: PropTypes.array,
   genreFilter: PropTypes.number,
   typeFilter: PropTypes.number,
   pagination: PropTypes.number,
   rangeMax: PropTypes.number,
};

export default MovieList