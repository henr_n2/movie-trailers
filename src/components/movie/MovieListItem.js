import React from 'react';
import PropTypes from 'prop-types';

const MovieListItem = ({movie, types, genres, selectTrailer}) => {
   let type = types[0] != null ? types[0] : null;
   return (
      <div className="movie-list-item">
         {movie && (
            <div className="movie-list-item--inner">
               <div className="movie-poster-container">
                  <img className="movie-poster" src={movie.image} alt={movie.title}  onClick={selectTrailer(movie)}/>
                  {type && (<span className="movie-type-card">{type.title}</span>)}
               </div>
               <p className="movie-title">{movie.title}</p>
               
               <div className="movie-info">
                  <p className="genres">
                     {genres.map((genre,index) => {
                        let colon;
                        if(index >= 0 && index < (genres.length - 1)) {
                           colon = ', '
                        } else {
                           colon = ''
                        }
                        return(
                           <span key={genre.id} className="genre">{genre.title}{colon}</span>
                        )
                     })}
                  </p>
                  <p className="runtime">{movie.duration}</p>
               </div>
            </div>
            
         )}
         
      </div>
   )
}

MovieListItem.propTypes = {
   movie: PropTypes.object,
   types: PropTypes.array,
   genres: PropTypes.array,
   selectTrailer: PropTypes.func,
};

export default MovieListItem;