export const SET_PAGE = 'SET_PAGE';

export const setPage = (page, rangeMax) => ({
   type: SET_PAGE,
   payload: { 
      pagination: page,
      rangeMax: rangeMax
   }
})