export const SET_GENRE_FILTER   = 'SET_GENRE_FILTER';
export const SET_TYPE_FILTER   = 'SET_TYPE_FILTER';
export const REMOVE_GENRE_FILTER   = 'REMOVE_GENRE_FILTER';
export const REMOVE_TYPE_FILTER   = 'REMOVE_TYPE_FILTER';

export const setGenreFilter = filter => ({
   type: SET_GENRE_FILTER,
   payload: { filter }
})

export const setTypeFilter = filter => ({
   type: SET_TYPE_FILTER,
   payload: { filter }
})

export const removeGenreFilter = () => ({
   type: REMOVE_GENRE_FILTER
})

export const removeTypeFilter = () => ({
   type: REMOVE_TYPE_FILTER
})