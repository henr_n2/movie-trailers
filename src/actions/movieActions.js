export const FETCH_MOVIES_BEGIN   = 'FETCH_MOVIES_BEGIN';
export const FETCH_MOVIES_SUCCESS = 'FETCH_MOVIES_SUCCESS';
export const FETCH_MOVIES_FAILURE = 'FETCH_MOVIES_FAILURE';

export const fetchMoviesBegin = () => ({
   type: FETCH_MOVIES_BEGIN
 });
 
 export const fetchMoviesSuccess = items => ({
   type: FETCH_MOVIES_SUCCESS,
   payload: { items }
 });
 
 export const fetchMoviesFailure = error => ({
   type: FETCH_MOVIES_FAILURE,
   payload: { error }
 });

 export function fetchMovies() {
   return dispatch => {
     dispatch(fetchMoviesBegin());
     return fetch('data/movies.json')
       .then(handleErrors)
       .then(r => r.json())
       .then(json => {
          console.log(json)
         dispatch(fetchMoviesSuccess(json));
         return json.products;
       })
       .catch(error => dispatch(fetchMoviesFailure(error)));
   };
 }
 
 // Handle HTTP errors since fetch won't.
 function handleErrors(response) {
   if (!response.ok) {
     throw Error(response.statusText);
   }
   return response;
 }