import { connect } from 'react-redux'
import MovieList from '../components/movie/MovieList'
import { removeGenreFilter, removeTypeFilter } from '../actions/filterActions'
import { setPage } from '../actions/paginationAction'

const mapStateToProps = (state, ownProps) => {
   let moviesList = state.movies.movies;
   /*
    Filter the movies by genre
    */
   if(state.filter.genre) {
      moviesList = moviesList.filter(movie => {
         return movie.genre.includes(state.filter.genre);
      })
   }
   /*
    Filter the movies by type
    */
   if(state.filter.type) {
      moviesList = moviesList.filter(movie => {
         return movie.type.includes(state.filter.type);
      })
   }
   /*
    Filter the movies by Search
    */
   if(state.search.search) {
      moviesList = moviesList.filter(movie => {
         const title = movie.title.toLowerCase()
         const search = state.search.search.toLowerCase()
         return title.includes(search)
      })
   }
   return {
      movies: state.movies.movies ? moviesList : [],
      types: state.movies.types ? state.movies.types : [],
      genres: state.movies.genres ? state.movies.genres : [],
      genreFilter: state.filter.genre,
      typeFilter: state.filter.type,
      pagination: state.pagination.pagination,
      rangeMax: state.pagination.rangeMax,
   }   
}
const mapDispatchToProps = (dispatch) => ({
   removeGenreFilter: ()  => dispatch(removeGenreFilter()),
   removeTypeFilter: ()  => dispatch(removeTypeFilter()),
   setPage: (page, rangeMax)  => dispatch(setPage(page, rangeMax)),
 })
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieList)