import { connect } from 'react-redux'
import { setSearch } from '../actions/searchbarAction'
import Searchbar from '../components/searchbar/Searchbar'

const mapStateToProps = (state, ownProps) => ({
  movies: state.movies.movies,
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  setSearch: search => dispatch(setSearch(search))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Searchbar)