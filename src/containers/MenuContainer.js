import { connect } from 'react-redux'
import { setGenreFilter, setTypeFilter } from '../actions/filterActions'
import { setPage } from '../actions/paginationAction'
import Menu from '../components/menu/Menu'

const mapStateToProps = (state, ownProps) => ({
  genres: state.movies.genres,
  types: state.movies.types
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  setGenre: filter => dispatch(setGenreFilter(filter)),
  setType: filter => dispatch(setTypeFilter(filter)),
  setPage: (page, rangeMax)  => dispatch(setPage(page, rangeMax)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Menu)