import { SET_SEARCH } from '../actions/searchbarAction'

const initialState = {
   search: null,
};

const search = (state = initialState, action) => {
   switch (action.type) {
      case SET_SEARCH:
         return {
            ...state,
            search: action.payload.search,
          }
      default:
         return state
   }
}

export default search