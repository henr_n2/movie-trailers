import { combineReducers } from "redux"
import movies from "./movieReducer";
import filter from "./filterReducer";
import search from "./searchReducer";
import pagination from "./paginationReducer";

export default combineReducers({
   movies,
   filter,
   search,
   pagination
})