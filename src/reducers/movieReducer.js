import {
   FETCH_MOVIES_BEGIN,
   FETCH_MOVIES_SUCCESS,
   FETCH_MOVIES_FAILURE
 } from '../actions/movieActions';

 const initialState = {
   movies: [],
   genres: [],
   types: [],
   loading: false,
   error: null
};

export default function movieReducer(state = initialState, action) {
   switch(action.type) {
     case FETCH_MOVIES_BEGIN:
       return {
         ...state,
         loading: true,
         error: null
       };
 
     case FETCH_MOVIES_SUCCESS:
       return {
         ...state,
         loading: false,
         movies: action.payload.items.movies,
         genres: action.payload.items.genres,
         types: action.payload.items.types,
       };
 
     case FETCH_MOVIES_FAILURE:
       return {
         ...state,
         loading: false,
         error: action.payload.error,
         items: []
       };
 
     default:
       return state;
   }
 }