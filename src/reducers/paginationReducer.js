import { SET_PAGE } from '../actions/paginationAction'

const initialState = {
   pagination: 1,
   rangeMax: 12,
};

const pagination = (state = initialState, action) => {
   switch (action.type) {
      case SET_PAGE:
         return {
            pagination: action.payload.pagination,
            rangeMax: action.payload.rangeMax,
         }
      default:
         return state
   }
}

export default pagination