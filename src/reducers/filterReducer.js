import { SET_GENRE_FILTER, SET_TYPE_FILTER, REMOVE_GENRE_FILTER, REMOVE_TYPE_FILTER } from '../actions/filterActions'

const initialState = {
   genre: null,
   type: null,
};

const visibilityFilter = (state = initialState, action) => {
   switch (action.type) {
      case SET_GENRE_FILTER:
         return {
            ...state,
            genre: action.payload.filter,
          }
      case SET_TYPE_FILTER:
         return {
            ...state,
            type: action.payload.filter,
          }
      case REMOVE_GENRE_FILTER:
         return {
            ...state,
            genre: null,
         }
      case REMOVE_TYPE_FILTER:
         return {
            ...state,
            type: null,
         }
      default:
         return state
   }
}

export default visibilityFilter